class CreateLocations < ActiveRecord::Migration[6.1]
  def change
    create_table :locations do |t|
      t.text :address_type
      t.text :address
      t.string :lat
      t.string :long
      t.references :user
      t.timestamps
    end
  end
end
