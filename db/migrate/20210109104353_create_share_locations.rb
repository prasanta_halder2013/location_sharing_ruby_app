class CreateShareLocations < ActiveRecord::Migration[6.1]
  def change
    create_table :share_locations do |t|
      t.references :share_by
      t.references :share_with
      t.references :location
      t.boolean :is_private
      t.timestamps
    end
  end
end
