Rails.application.routes.draw do

	devise_for :users, controllers: { sessions: 'sessions' }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  
  devise_scope :user do
      root to: 'sessions#new'
  end

  resources :dashboard, only: :index
  resources :locations do
  	collection do
  		post :search
  	end
    resources :share_locations, only: [:new, :create]
  end
  resources :share_locations, only: [:index, :destroy]
end
