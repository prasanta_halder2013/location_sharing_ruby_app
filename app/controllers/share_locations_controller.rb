class ShareLocationsController < ApplicationController

	before_action :authenticate_user!, :current_user

	def index
		@shared_locations = @current_user.shared_by_me.includes(:location)
	end

	def new
		@location = @current_user.locations.find(params[:location_id])
		@shared_location = ShareLocation.new()
	end

	def create
		@location = @current_user.locations.find(params[:location_id])
		if params[:share_location][:is_private] == "true"
			params[:share_location][:share_with_id].each do |user_id|
				if user_id.present?
					@shared_location = ShareLocation.create!({
						share_by_id: @current_user.id,
						share_with_id: user_id,
						location: @location,
						is_private: true
					})
				end
			end
		else
			@shared_location = ShareLocation.create!({
				share_by_id: @current_user.id,
				location: @location,
				is_private: false
			})
		end

		redirect_to share_locations_path
	end

	def destroy
		@shared_location = ShareLocation.find(params[:id])
		@shared_location.destroy!
		redirect_to share_locations_path
	end

end
