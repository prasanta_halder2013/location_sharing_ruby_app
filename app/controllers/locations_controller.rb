class LocationsController < ApplicationController
	before_action :authenticate_user!, :current_user

	def index
		@locations = @current_user.locations
		@private_locations = @current_user.shared_with_me.try(:private_share).includes(:location)
		@public_locations = ShareLocation.includes(:location).where(is_private: false)
	end

	def search
		
	end

	def new
		@location  = Location.new
	end

	def create
		@location  = @current_user.locations.create!(location_params)
		redirect_to locations_path
	end


	def destroy
		@location  = @current_user.locations.find(params[:id])
		@location.destroy!
		redirect_to locations_path
	end

	def show
		@location  = @current_user.locations.find(params[:id])
	end

	def update
	end

	private

	def location_params
		params.require(:location).permit(:lat, :long, :address, :address_type)
	end
end
