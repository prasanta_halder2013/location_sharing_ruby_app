class ApplicationController < ActionController::Base

	def after_sign_in_path_for(resource)
	  dashboard_index_path # your path
	end

	def current_user
	   @current_user ||= super || User.find(@current_user_id)
	end
end
