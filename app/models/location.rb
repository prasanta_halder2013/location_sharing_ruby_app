class Location < ApplicationRecord

	belongs_to :user
	has_many :share_location, dependent: :destroy
end
