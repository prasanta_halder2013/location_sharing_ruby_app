class ShareLocation < ApplicationRecord

	belongs_to :share_by, foreign_key: 'share_by_id', class_name: 'User'
	belongs_to :share_with, foreign_key: 'share_with_id', class_name: 'User', optional: true
	belongs_to :location

	scope :private_share, -> {where(is_private: true)}
  	scope :public_share, -> {where(is_private: false)}
end
