class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable
  # , :registerable,:recoverable, :rememberable, :validatable

  has_many :locations
  has_many :shared_by_me, foreign_key: 'share_by_id', class_name: 'ShareLocation'
  has_many :shared_with_me, foreign_key: 'share_with_id', class_name: 'ShareLocation'
end
